import React from "react";
import "bootstrap/dist/css/bootstrap.css";
import axios from "axios";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";

const SignupSchema = Yup.object().shape({
  name: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Name is Required"),

  profilepic: Yup.mixed().required("Profile picture is required"),

  documentPic: Yup.mixed().required(),

  phonenumber: Yup.string()
    .matches(
      /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/,
      "Phone number is not valid"
    )
    .required("Phone number is Required"),

  zipcode: Yup.string().required("Zipcode is Required"),

  email: Yup.string()
    .email("Invalid email")
    .required("Email is Required"),

  address: Yup.string().required("Address id Required")
});

const UserForm = () => (
  <div className="container">
    <div className="row mb-5">
      <div className="col-lg-12 text-center">
        <h1 className="mt-5">User Form</h1>
      </div>
    </div>
    <div className="row">
      <div className="col-lg">
        <Formik
          initialValues={{
            name: "",
            email: "",
            address: "",
            phonenumber: "",
            zipcode: "",
            profilepic: "",
            documentPic: ""
          }}
          validationSchema={SignupSchema}
          onSubmit={values => {
            const {
              name,
              email,
              address,
              phonenumber,
              zipcode,
              profilepic,
              documentPic
            } = values;
            var formData = new FormData();
            let query = `mutation UserInfo($name:String!,$email:String!,$address:String!,$phonenumber:String!,$zipcode:String!,
      $profilepicture: Upload!,$docproof: Upload!) {
      UserInfo(name: $name,email:$email,address: $address,phonenumber:$phonenumber,zipcode: $zipcode,profilepicture: $profilepicture,docproof: $docproof)
    }`;
            const project = {
              name: name,
              email: email,
              address: address,
              phonenumber: phonenumber,
              zipcode: String(zipcode),
              profilepicture: null,
              docproof: null
            };

            const operations = JSON.stringify({
              query,
              variables: { ...project }
            });
            formData.append("operations", operations);
            const map = {
              "0": ["variables.profilepicture"],
              "1": ["variables.docproof"]
            };
            formData.append("map", JSON.stringify(map));
            formData.append("0", profilepic);
            formData.append("1", documentPic);

            axios({
              method: "POST",
              url: `http://127.0.0.1:3000/graphql`,
              data: formData,
              headers: {
                "content-type": `multipart/form-data; boundary=${formData._boundary}`
              }
            });
          }}
        >
          {({
            errors,
            touched,
            values,
            handleChange,
            handleSubmit,
            setFieldValue
          }) => {
            return (
              <form onSubmit={handleSubmit} encType="multipart/form-data">
                <div className="form-group">
                  <label>User Name</label>
                  <input
                    type="text"
                    name="name"
                    className={`form-control ${
                      errors.name && touched.name ? "is-invalid" : ""
                    }`}
                    placeholder="John Doe"
                    onChange={handleChange}
                    value={values.name}
                  />
                  <div className="invalid-feedback">{errors.name}</div>
                </div>
                <div className="form-group">
                  <label>Email address</label>
                  <input
                    type="email"
                    name="email"
                    className={`form-control ${
                      errors.email && touched.email ? "is-invalid" : ""
                    }`}
                    placeholder="someone@example.com"
                    onChange={handleChange}
                    value={values.email}
                  />
                  <div className="invalid-feedback">{errors.email}</div>
                </div>

                <div className="form-group">
                  <label>Phone Number</label>
                  <input
                    type="text"
                    name="phonenumber"
                    className={`form-control ${
                      errors.phonenumber && touched.phonenumber
                        ? "is-invalid"
                        : ""
                    }`}
                    placeholder=" (123) 456-7899 "
                    onChange={handleChange}
                    value={values.phonenumber}
                  />
                  <div className="invalid-feedback">{errors.phonenumber}</div>
                </div>

                <div className="form-group">
                  <label>Your Address</label>
                  <input
                    type="text"
                    name="address"
                    placeholder="Your Address"
                    className={`form-control ${
                      errors.address && touched.address ? "is-invalid" : ""
                    }`}
                    onChange={handleChange}
                    value={values.address}
                  />
                  <div className="invalid-feedback">{errors.address}</div>
                </div>
                <div className="form-group">
                  <label>Your Zip Code</label>
                  <input
                    type="number"
                    name="zipcode"
                    className={`form-control ${
                      errors.zipcode && touched.zipcode ? "is-invalid" : ""
                    }`}
                    placeholder="Your Zip Code"
                    onChange={handleChange}
                    value={values.zipcode}
                  />
                  <div className="invalid-feedback">{errors.zipcode}</div>
                </div>

                <div className="form-group">
                  <label>Your Profile Photo</label>
                  <input
                    type="file"
                    name="profilepic"
                    className={`form-control ${
                      errors.profilepic && touched.profilepic
                        ? "is-invalid"
                        : ""
                    }`}
                    onChange={event => {
                      setFieldValue("profilepic", event.currentTarget.files[0]);
                    }}
                  />
                  <div className="invalid-feedback">{errors.profilepic}</div>
                </div>

                <div className="form-group">
                  <label>Your Documet Proof (Licence , ID card)</label>
                  <input
                    type="file"
                    name="documentPic"
                    className={`form-control ${
                      errors.documentPic && touched.documentPic
                        ? "is-invalid"
                        : ""
                    }`}
                    onChange={event => {
                      setFieldValue(
                        "documentPic",
                        event.currentTarget.files[0]
                      );
                    }}
                  />
                  <div className="invalid-feedback">{errors.documentPic}</div>
                </div>

                <button
                  type="submit"
                  className="btn btn-primary btn-block"
                  value="SAVE"
                >
                  SAVE
                </button>
              </form>
            );
          }}
        </Formik>
      </div>
    </div>
  </div>
);
export default UserForm;
