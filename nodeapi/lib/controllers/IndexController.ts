const _ = require('underscore');
import {
    Controller, Authorized, BodyParam, Req, Res,
    Body, Get
} from "routing-controllers";
import { InjectConnection } from "typeorm-typedi-extensions";
import { Connection } from "typeorm";


@Controller()
export class IndexController {

    @InjectConnection()
    public connection: Connection;

    @Get("/")
    async indexHome(@Req() req: any, @Res() resp: any) {
        return "ok"

    }
}