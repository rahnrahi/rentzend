import app from "./app";
const PORT = 3000;
import { createConnection, useContainer as useContainerTypeORM } from "typeorm";
import { Container } from "typedi";
import {config} from './config'
useContainerTypeORM(Container);


createConnection({
    type: "mysql",
    host: config.MYSQL_HOST,
    port: config.MYSQL_PORT,
    username: config.MYSQL_USER,
    password: config.MYSQL_PASSWORD,
    database: `upload_test`,
    synchronize: true,
    logging: false,
    entities: [
        __dirname + "/entity/*"
    ]
}).then(connection => {

    const server = app.listen(PORT, function () {
        console.log(`Listening on port ${PORT}...`);
    });
    server.setTimeout(500000);

}).catch(e=>{
    console.log("Server error",e);
});
