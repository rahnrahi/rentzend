import {
  Entity,
  Column,
  OneToMany,
  PrimaryGeneratedColumn,
  OneToOne,
  BaseEntity
} from "typeorm";
import { ObjectType, Field, ID, Int, ArgsType } from "type-graphql";

@ObjectType()
@ArgsType()
@Entity()
export class Users extends BaseEntity {
  @Field(() => ID, { nullable: true })
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column()
  name: string;

  @Field()
  @Column()
  email: string;

  @Field()
  @Column()
  phonenumber: string;

  @Field()
  @Column()
  address: string;

  @Field()
  @Column()
  zipcode: string;

  @Column()
  profilephoto: string;

  @Column()
  docproof: string;
}
