import {
  Resolver,
  Query,
  Mutation,
  Arg,
  Ctx, Authorized, Args
} from "type-graphql";
const { ApolloError } = require('apollo-server-express');
import { Users } from "../../entity/Users";
import { Connection } from "typeorm";
import { InjectConnection } from "typeorm-typedi-extensions";
import { Request, Response } from 'express'
import { config } from '../../config'

var _ = require('underscore');
import { Inject, Service } from "typedi";
import * as async from "async";
import { Upload } from "../types/UserTypes";
import { GraphQLUpload } from "graphql-upload";
import { createWriteStream } from "fs";
var path = require("path");



@Service()
@Resolver(Users)
export class UserResolver {

  @InjectConnection()
    public connection: Connection;


  @Query(() => [Users])
  async getUsers() {
    return await Users.find() 
  }

  @Mutation(() => String)
  UserInfo(
    @Args() {name, address, email, phonenumber, zipcode} : Users,
    @Arg("profilepicture", () => GraphQLUpload) !profilepicture : Upload,
    @Arg("docproof", () => GraphQLUpload) !docproof: Upload
  ) {
    return new Promise(async (resolve, reject) => {
      try {
        await profilepicture
          .createReadStream()
          .pipe(
            createWriteStream(
              __dirname + `/../../asset/images/${profilepicture.filename}`
            )
          );
        await docproof
          .createReadStream()
          .pipe(
            createWriteStream(
              __dirname + `/../../asset/images/${docproof.filename}`
            )
          );
        let r = await this.connection
          .createQueryBuilder()
          .insert()
          .into(Users)
          .values([
            {
              name: name,
              address: address,
              email: email,
              phonenumber: phonenumber,
              zipcode: zipcode,
              profilephoto: profilepicture.filename,
              docproof: docproof.filename
            }
          ])
          .execute();
        return resolve("okk");
      } catch (error) {
        throw new ApolloError(error, 300);
        return reject(error);
      }
    });
  }

}
