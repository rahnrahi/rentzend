import { UserResolver } from "./resolvers/UserResolver";

const { ApolloServer } = require('apollo-server-express');
import { buildSchema } from "type-graphql";
let promises = require('bluebird');
import { Container } from "typedi";
import { config } from '../config'


export default async function (app) {

  const schema = await buildSchema({
    resolvers: [UserResolver],
    container: Container
  });

  let apiGraphQl =  'http://127.0.0.1:3000';
  const apolloserver = new ApolloServer({
    schema,
    tracing: false,
    playground: {
      endpoint: `${apiGraphQl}/graphql`
    }
  });

  // The GraphQL endpoint
  apolloserver.applyMiddleware({
    app,
    cors: {
      origin: config.ALLOWED_URL[0],            // <- allow request from  domain
      credentials: true
    },
    path: '/graphql'
  });


};