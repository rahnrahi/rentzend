import * as express from "express";
import * as bodyParser from "body-parser";
import * as path from 'path';
import * as cookieParser from 'cookie-parser';
import {config} from './config'
import "reflect-metadata"; // this shim is required
import {useExpressServer,Action, useContainer} from "routing-controllers";
import {Container} from "typedi";
let promises = require('bluebird');
import ApolloServerfrom from "./graphql/ApolloServer";


useContainer(Container);

class App {

    public app: express.Application;
   
    constructor() {
         this.app =  express();
         this.config(); 
    }

    private config(): void{
        this.app.use(cookieParser());
        this.app.disable('etag');
        this.app.use(express.json({limit: '50mb'}));
        this.app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

        this.app.use(function (req, res, next) {

            var origin : any = req.headers.origin;
         
            if(config.ALLOWED_URL.indexOf(origin) > -1){
                res.setHeader('Access-Control-Allow-Origin', origin);
            }
          
            // Request methods you wish to allow
            res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
          
            // Request headers you wish to allow
            res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
          
            res.setHeader('Access-Control-Allow-Credentials', 'true');
          
            // Pass to next layer of middleware
            next();
        });

        this.app.use( express.static(path.join(__dirname, "asset")));

        ApolloServerfrom(this.app);

        useExpressServer(this.app, { // register created express server in routing-controllers
            classTransformer: true,
            controllers: [
                __dirname+"/controllers/*"
            ] 
        }); 
        
      
       

        
    }

}

export default new App().app;