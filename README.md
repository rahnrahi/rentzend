# RentZend’s Engineering Team Interview
# nodeapi folder contains backend api with GraphQL, Apollo Server , sequelize.

  - Change the mysql configuration from nodeapi/config/index.ts
  - Run npm i to install all the dependency
  - npm start will start the node api at port 3000
  - GraphQL playground runs at http://127.0.0.1:3000/graphql 

# ui folder contains frontend 

  - Run npm i to install all the dependency
  - npm start will start the node api at port 3010
 
 # Access the ui by using http://127.0.0.1:3010/ not http://localhost:3010/ due to cors policy issue





